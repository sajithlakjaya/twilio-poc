﻿using System.Collections.Generic;
using Twilio.Jwt.AccessToken;

namespace TwilioPoc.Intergration.Twilio
{
    public class TwilioTokenService
    {
        private readonly string _accountSid;
        private readonly string _signingKey;
        private readonly string _secret;
        public TwilioTokenService(string accountSid, string signingKey, string secret)
        {
            _accountSid = accountSid;
            _signingKey = signingKey;
            _secret = secret;
        }

        public string GenerateToken(string identity, HashSet<IGrant> grants)
        {
            var token = new Token(
                _accountSid,
                _signingKey,
                _secret,
                identity: identity,
                grants: grants);

            return token.ToJwt();
        }
    }
}
