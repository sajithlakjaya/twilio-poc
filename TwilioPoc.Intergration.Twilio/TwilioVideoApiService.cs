﻿using System.Threading.Tasks;
using Twilio;
using Twilio.Exceptions;
using Twilio.Rest.Video.V1;
using TwilioPoc.Intergration.Twilio.Core;

namespace TwilioPoc.Intergration.Twilio
{
    public class TwilioVideoApiService
    {
        public TwilioVideoApiService(string apiKeySid, string apiKeySecret)
        {
            TwilioClient.Init(apiKeySid, apiKeySecret);
        }

        public async Task<string> CreateRoom(string roomName)
        {
            try
            {
                var room = await RoomResource.CreateAsync(uniqueName: roomName,
                    type: RoomResource.RoomTypeEnum.PeerToPeer);
                return room.Sid;
            }
            catch (TwilioException ex)
            {
                throw new VideoApiException(ex.Message);
            }
        }

        public async Task CompleteRoom(string sid)
        {
            try
            {
                await RoomResource.UpdateAsync(sid,
                    RoomResource.RoomStatusEnum.Completed);
            }
            catch (TwilioException ex)
            {
                throw new VideoApiException(ex.Message);
            }
        }
    }
}
