﻿using System;

namespace TwilioPoc.Intergration.Twilio.Core
{
    public class VideoApiException: Exception
    {
        public VideoApiException(string message): base(message)
        {
        }
    }
}
