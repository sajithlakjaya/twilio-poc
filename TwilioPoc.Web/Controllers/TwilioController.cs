﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Twilio.Jwt.AccessToken;
using TwilioPoc.Intergration.Twilio;
using TwilioPoc.Web.Models;

namespace TwilioPoc.Web.Controllers
{
    [RoutePrefix("api/twilio")]
    public class TwilioController : ApiController
    {
        private readonly TwilioTokenService _twilioTokenService;
        private readonly TwilioVideoApiService _twilioVideoApiService;
        public TwilioController(TwilioTokenService twilioTokenService, TwilioVideoApiService twilioVideoApiService)
        {
            _twilioTokenService = twilioTokenService;
            _twilioVideoApiService = twilioVideoApiService;
        }

        [Route("token")]
        [HttpGet]
        public IHttpActionResult GetToken(string roomName)
        {
            //Generate the token for user, you should pass a unique user id for that method
            //Right now i'm generate guid and pass to generate token method to get access token to connect to room
            var token = _twilioTokenService.GenerateToken($"{Guid.NewGuid()}",
                new HashSet<IGrant>() { new VideoGrant() { Room = roomName } });
            return Ok(new
            {
                token = token
            });
        }

        [Route("closeroom")]
        [HttpPost]
        public async Task<IHttpActionResult> CloseRoom(CloseRoomBindingModal modal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //Change the status to compelte which is created room.
            await _twilioVideoApiService.CompleteRoom(modal.Sid);
            return Ok();
        }
    }
}
