﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using TwilioPoc.Intergration.Twilio;

namespace TwilioPoc.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly TwilioVideoApiService _twilioVideoApiService;
        public HomeController(TwilioVideoApiService twilioVideoApiService)
        {
            _twilioVideoApiService = twilioVideoApiService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> CreateRoom()
        {
            var roomId = Guid.NewGuid();
            var sid = await _twilioVideoApiService.CreateRoom(roomId.ToString());
            return RedirectToAction("Room", new { rid = roomId, sid = sid });
        }

        public ActionResult Room(string rid, string sid)
        {
            ViewBag.RoomName = rid;
            ViewBag.Sid = sid;
            return View();
        }
    }
}