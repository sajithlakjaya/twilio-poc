﻿var videoService = {

    connectToRoom: function (roomName, remoteMediaElement, localMediaElement) {

        var self = this;
        self._loadingRemote(remoteMediaElement);

        fetch('/api/twilio/token?roomName=' + roomName,
            {
                headers: {
                    'content-type': 'application/json'
                },
                method: 'GET'
            })
            .then(response => response.json())
            .then(data => {
                self._initTwilio(data.token, roomName, remoteMediaElement, localMediaElement);
            });

    },

    disconnect: function (sid) {

        fetch('/api/twilio/closeroom',
            {
                headers: {
                    'content-type': 'application/json'
                },
                method: 'POST',
                body: JSON.stringify({
                    sid: sid
                })
            })
            .then(function () {
                window.location.href = '/';
            }).catch(function () {
                self._showMessage({
                    title: "Error!",
                    text: "Unable to disconnect from room",
                    icon: "error"
                });
            });
    },

    _participantConnect: function (participant, remoteMediaElement) {

        var self = this;

        self._showMessage({
            title: "Participant connected to room!",
            text: "Your participant connected to room!",
            icon: "success"
        });

        participant.tracks.forEach(track => {
            remoteMediaElement.appendChild(track.attach());
        });

        participant.on('trackAdded', track => {
            remoteMediaElement.appendChild(track.attach());
        });

        participant.on('trackRemoved', track => {
            var attachedElements = track.detach();
            attachedElements.forEach(element => element.remove());
        });

        participant.on('disconnected', track => {
            participant.tracks.forEach(track => {
                var attachedElements = track.detach();
                attachedElements.forEach(element => element.remove());
            });

            self._loadingRemote(remoteMediaElement);
            self._showMessage({
                title: "Disconnected!",
                text: "Your participant disconnected",
                icon: "warning"
            });
        });

    },

    _loadingRemote: function (remoteMediaElement) {
        $(remoteMediaElement).LoadingOverlay("show", {
            background: "rgb(245, 245, 245)",
            zIndex: 0
        });
    },

    _hideLoadingRemote: function (remoteMediaElement) {
        $(remoteMediaElement).LoadingOverlay("hide", true);
    },

    _showMessage: function(msgData) {
        return swal(msgData);
    },

    _initTwilio(token, roomName, remoteMediaElement, localMediaElement) {

        var self = this;

        Twilio.Video.createLocalTracks().then(function (localTracks) {

            var localMediaContainer = localMediaElement;

            localTracks.forEach(function (track) {
                localMediaContainer.appendChild(track.attach());
            });

            return Twilio.Video.connect(token, { name: roomName, tracks: localTracks }).then(function (room) {

                self._showMessage({
                    title: "Joined to room!",
                    text: "Successfully joined to Room!",
                    icon: "success"
                });

                room.participants.forEach(participant => {
                    self._hideLoadingRemote(remoteMediaElement);
                    self._participantConnect(participant, remoteMediaElement);
                });

                room.on('participantConnected', participant => {
                    self._hideLoadingRemote(remoteMediaElement);
                    self._participantConnect(participant, remoteMediaElement);
                });

                room.on('disconnected', function (room, error) {

                    room.localParticipant.tracks.forEach(function (track) {
                        track.stop();
                        track.detach();
                    });

                    self._showMessage({
                        title: "Room Closed!",
                        text: "Your room is closed",
                        icon: "warning"
                    }).then(function () {
                        window.location.href = '/';
                    });

                });

            }, function (error) {

                self._showMessage({
                    title: "Unable to connect to Room!",
                    text: error.message,
                    icon: "error"
                });

            });

        });
    }
}



