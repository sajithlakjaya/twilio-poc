﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Twilio.Jwt.AccessToken;

namespace TwilioPoc.Intergration.Twilio.Test
{
    [TestClass]
    public class TwilioTokenServiceTest
    {
        private readonly string accountSid = "ACa7bb72e0942cf16e8571c8059147b8f5";
        private readonly string signingKey = "SKcc777bbfc0dd2b84f16f4c697ae65292";
        private readonly string secret = "b9246770377eeb3330f8e9acf66737e6";

        [TestMethod]
        public void Generate_Token_Test()
        {
            var twilioTokenService = new TwilioTokenService(accountSid, signingKey, secret);
            var token = twilioTokenService.GenerateToken("user",
                new HashSet<IGrant>() {new VideoGrant() {Room = "test-room"}});
            Assert.AreNotEqual(token, null);
        }
    }
}
