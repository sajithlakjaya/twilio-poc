﻿using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TwilioPoc.Intergration.Twilio.Core;

namespace TwilioPoc.Intergration.Twilio.Test
{
    [TestClass]
    public class TwilioVideoApiServiceTest
    {
        private readonly string accountSid = "ACa7bb72e0942cf16e8571c8059147b8f5";
        private readonly string authToken = "b9246770377eeb3330f8e9acf66737e6";

        [TestMethod]
        public async Task Test_Create_Room_And_Close_Room()
        {
            var twilioVideoApiService = new TwilioVideoApiService(accountSid, authToken);
            var roomSid = await twilioVideoApiService.CreateRoom("test-room");
            Assert.AreNotEqual(roomSid, null);
            await twilioVideoApiService.CompleteRoom(roomSid);
        }

        [TestMethod]
        public async Task Test_Invalid_Status_Room_Closing()
        {
            var twilioVideoApiService = new TwilioVideoApiService(accountSid, authToken);
            await Assert.ThrowsExceptionAsync<VideoApiException>(async () =>
            {
                await twilioVideoApiService.CompleteRoom("RM3533d2a294f0faf0a5a070f8a6bf96ce");
            }, "Should throw an exception");
        }

        [TestMethod]
        public async Task Test_Create_Duplicate_Room()
        {
            var twilioVideoApiService = new TwilioVideoApiService(accountSid, authToken);
            var roomSid = await twilioVideoApiService.CreateRoom("test-room-duplicate");

            await Assert.ThrowsExceptionAsync<VideoApiException>(async () =>
            {
                await twilioVideoApiService.CreateRoom("test-room-duplicate");
            }, "Should throw an exception");

            await twilioVideoApiService.CompleteRoom(roomSid);
        }
    }
}
